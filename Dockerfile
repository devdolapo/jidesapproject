FROM node:14.2
WORKDIR /var/app 
copy . /var/app
RUN npm install
CMD ["node", "app.js"]


